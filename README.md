# docker-spring-boot-gradle-demo

A demonstration of a Docker-based application, utilizing Spring Boot as an application framework, Gradle for build automation, and a project level Gitlab Container Registry as a container registry.
